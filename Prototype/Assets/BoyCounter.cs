﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoyCounter : MonoBehaviour {

    static int BoysLeft = 5;

	// Use this for initialization
	void Start () {
        print("Find the Boys. Total Boys = 5");
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void OnMouseDown()
    {
        print("Boy Found!");
        BoysLeft -= 1;
        Destroy(this.gameObject);

        if (BoysLeft <= 0)
        {
            print("All Boys Found");
        }

    }
}
